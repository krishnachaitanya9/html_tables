class HTML_Page():
    html_headings = """<!DOCTYPE html>
                            <html lang="en">
                            <head>
                              <title>OITCCure</title>
                              

                              <style type="text/css">

 
                                    caption {
                                              background: #fff;
                                              //padding: 15px; 
                                              caption-side: top;
                                              font-size: 30px;
                                            }

                                  th, td {
                                      padding: 5px;                 
                                      text-align: center;
                                      
                                    }
                                    
                                th, td, table {
                                      border-collapse: collapse;
                                    }

                                  tr:hover {background-color: #e5e5e5;}

                                   




                                </style>
                            </head>
                            <body>
                            
    """

    intercept_data = "<pre> {intercept_text} </pre>"
    extra_data = ""

    html_ending = """

                                </body>
                                </html>  

        """

    tables_list_string = ""

    def add_table(self, table):
        self.tables_list_string += table.table_heading + table.table_columns + table.table_endings

    def add_intercept(self, intercepts):
        self.intercept_data = self.intercept_data.format(intercept_text=intercepts)

    def add_extradata(self, extra_data):
        self.extra_data += extra_data


    def save_table(self):
        open('test.html', 'w').close()
        with open('test.html', 'a') as htmlfile:
            if self.intercept_data != "<pre> {intercept_text} </pre>":
                htmlfile.write(self.html_headings +  self.tables_list_string + self.intercept_data + self.extra_data +self.html_ending)
            else:
                htmlfile.write(self.html_headings + self.tables_list_string + self.extra_data +self.html_ending)



class HTML_table():
    table_heading = """
                                <table class="table table-bordered table-hover" border="1">
                                    <caption class="text-center">{table_description}</caption>
                                      <thead class="thead-dark text-center lead">

                                          {add_headings}

                                      </thead>
                                      <tbody> 


    """
    table_columns = ""

    table_endings = """</tbody>
                        </table>"""

    def __init__(self, heading_list, table_description):
        self.table_heading = self.table_heading.format(table_description=table_description,
                                                       add_headings=self.get_heading_text(heading_list))

    def get_heading_text(self, column_list):
        column_tags = ""
        for every_element in column_list:
            htmltag = """<th>{element}</th>\n""".format(element=every_element)
            column_tags += htmltag

        final_table_text = column_tags
        return final_table_text

    def get_column_text(self, column_list):
        preceeding_text = """<tr class="text-center">"""
        succeeding_text = """</tr>"""
        column_tags = ""
        for every_element in column_list:
            htmltag = """<td>{element}</td>\n""".format(element=every_element)
            column_tags += htmltag

        final_table_text = preceeding_text + column_tags + succeeding_text
        return final_table_text

    def add_column(self, column_list):
        self.table_columns += self.get_column_text(column_list)